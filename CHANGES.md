## IN DEVELOPMENT

* Removed: Binary assembly files and VS(Code) cache files.

## [v0.0.0 - October 11, 2022 be8a2aff](https://gitlab.com/N3X15/rimworld-more-injuries/-/commit/be8a2aff236637d5b46017f580bfd105ac42b783)

* Initial commit, including binaries and VS(Code) cruft.