# More Injuries

A RimWorld mod by Caulaflower [sic] that was [removed from the workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2515060094). for some reason.

## LEGAL NOTE

* I, N3X15, am not the creator nor a contributor to the original plugin.
* This repository exists for the purpose of preservation and maintenance of the plugin, since removing it broke my 1.3 saves. I have made it publically available in case anyone else needs it.
* This is not intended to take any power from Caulaflower and their contributors, nor is it intended to cause them any problems.
* If asked by Caulaflower, I will remove this repository.
* If Caulaflower uploads More Injuries again, I will remove this repository and link to their implementation.
* I ask that users do not harass or bully past contributors for their actions.  Caulaflower likely has good reason for dropping this project, for example, as real-life issues can cause major shifts in priorities.

## License

**Licensing information was not mentioned in the original README, so I will be using the MIT Open-Source License for the time being.**  This may change at any time, should Caulaflower or their contributors request it.

Contributions via Merge Requests are welcome.

## Changes from Original Mod

See [CHANGES.md](CHANGES.md).

## Current Contributors

* N3X15 - Maintainer

## Past Contributions

* Caulaflower

# Original README

*Mildly reformatted to work with Markdown - N3X15*

## Features

Adds:

* bone fractures
* femoral arteries
* hemostats
* abdomen
* intestines
* instesinte/stomach spills when shot in the bodypart
* adrenaline levels lowering pain and increasing movement when dumped
* finally well working concussions
* lung collapses
* spalling. That means bulllet fragments from deflected ammo wounding the pawn
* airway clogging with blood, which must be cleared with airway suction device unlocked after medicine production
* shock
* hearing damage from gunfire
* tourniqettes, appliable through right click on pawn or gizmo when the pawn is downed
* hemorrhagic stroke, which is like, brain bleed happening because of too high pressure in blood vessels after high energy impact
* shock from blood loss mechanics
* bandages

### Planned features

* some grenade buffs with things like concussions or blinding

## Compatibility

CE compatible.

## SAVE GAME ISSUE

When adding the mod to an existing save, you are likely to run into an issue where bionics are misplaced, To fix this, load a map (the mod option doesn't show otherwise) and click option "Fix misplaced bionics". This will only affects pawns in loaded map, so you'll have to repeat the process if you are running a few maps

## Credits

* Caulaflower - idea, most of the code
* Blyatman (Epicguru) - harmony patch that made this possible
* Graved - this greatly epic thumbnail
* ShakestheSpeare - the CE beanos
* Magenta Ivy - ASD and splint graphics
* Ogam - hemostat graphics
* Rick G - tourniquets graphics

## Some plugging lol
*Link reportedly broken - N3X15*

Caulaflower's discord server [https://discord.gg/QaXJRujUMP](https://discord.gg/QaXJRujUMP)
